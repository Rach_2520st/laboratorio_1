#!/bin/bash
echo "nombre del directorio a crear:"
read CARPETA


if [[ ! -d "$HOME/$CARPETA" ]]; then
	echo "El directorio $CARPETA será creado"
	mkdir "$HOME/$CARPETA"
else 
	existente="$CARPETA"
	echo "El directorio que desea crear ya existe"
	echo "ingrese un nuevo nombre:"
	read -p "> " CARPETA
	echo "El directorio se cambió a $CARPETA"
	mkdir "$HOME/$CARPETA"
	
fi
cd $HOME/$CARPETA
touch 1t4v_docking.mol2 1t4v_referencia.mol2 2r2m_docking.mol2 2r2m_referencia.mol2 3ldx_docking.mol2 3ldx_docking_new.mol2 3ldx_referencia.mol2 3ldx_referencia_new.mol2

for x in *; do
	echo $x | awk '{print substr($0,1,1) }' | xargs mkdir -p
done

for x in *; do
	if [[ ! -d "$x" ]];then
	
		echo $x | awk '{print substr($0,1,1) }' | xargs mv $x
	fi
done


	
