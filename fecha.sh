#!/bin/bash
comando="$1"
dia=`date +%d`
mes=`date +%m`
ano=`date +%Y`

if [[ $# > 1 ]]; then 
	echo "solo se admite un parámetro"
	exit 1
fi 

if [[ $comando == "-s" || $comando == "--short" ]]; then
	date +'%d/%m/%Y'
elif [[ $comando == "-l" || $comando == "--long" ]]; then
	echo "Hoy es el dia $dia del mes $mes del año $ano"
else
	cal
fi

if [[ $comando != "-s" && $comando != "--short" && $comando != "-l" && $comando != "--long" ]]; then
	echo " Opción incorrecta, solo se acepta el parámetro −s(--short) o −l(--long)"
	exit 1
fi
